# Get Jabbed - Backend

## Project description

All backend repositories are placed in https://gitlab.com/get-jabbed.
All backend related repositories have "backend-" prefix.
In repositories without "-webservice" there are maven modules which consist code shared between all webservices:
 - backend-parent - The component is made for being a parent of all backend components and webservices.
 - backend-bom - The component is Bill of Materials for all backend components and webservices. This is made for keeping consistent versions of each
   component along all backend services.
 - backend-db - The component is made to set up spring-boot-starter-data-mongodb-reactive and Mongock (tool for db migrations - creating collection, indexes etc.)
   - it exposes 'db-spring-boot-starter' maven module
 - backend-webclient - The component is made to set up HTTP webclient
   - it exposes 'webclient-spring-boot-starter' maven module
 - backend-utils - The component is made to create few utils classes used in tests of all components and webservices
   - it exposes 'utils-test-spring-boot-starter'
 - backend-security -The component is made to set up security layer for Get Jabbed backend
   - it exposes 'security-spring-boot-starter'
 - backend-webservice-parent - The component is an aggregator of all above and it is used as a parent maven project for all webservices.
Rest of repositories are webservices, which expose secured REST API.
 - backend-user-webservice - to create users (patients and vaccine center users) and acquire JWT token
 - backend-vaccine-center-webservice - to create vaccine center
 - backend-appointment - to create appointments, find and book appointments etc. (not all features are implemented)
   - it creates three separate docker images:
     - appointment-webservice
     - find-appointment-webservice
     - book-appointment-webservice

## CI
All repositories have CI configured on the GitLab.
For components it deploys them to the Maven Repository on the Gitlab.
For webservice it pushs it to the Docker Registry on the Gitlab.

## CD
My plan is to use FluxCD on the Kubernetes cluster. Everything will be coded in Terragrunt/Terraform.
